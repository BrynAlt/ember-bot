"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bot_command_1 = __importDefault(require("../../types/akairo-extensions/bot-command"));
/**
 * Defines how to respond to _purge.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Command | akairo.Command}
 */
class PurgeCommand extends bot_command_1.default {
    constructor() {
        super("purge", {
            aliases: ["purge", "pg"],
            category: "mod",
            channel: "guild",
            clientPermissions: ["BAN_MEMBERS"],
            args: [{ id: "member", type: "member" }],
        });
    }
    /**
     * Main execution procedure for _purge.
     *
     * @remarks
     * This is required by Akairo
     *
     * @param message - Will contain the {@link https://discord.js.org/#/docs/main/stable/class/Message | Message} object that hooked the command.
     * @param member - The {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} mentioned in the command call to be purged.
     * @returns - A message indicating that the {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} has been purged.
     */
    async exec(message, { member }) {
        return member
            .ban({ days: 7 })
            .then((bannedMember) => Promise.resolve(`<@${message.author.id}> has purged **${bannedMember.user.tag}**`));
    }
}
exports.default = PurgeCommand;
//# sourceMappingURL=purge.js.map