"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bot_command_1 = __importDefault(require("../../types/akairo-extensions/bot-command"));
/**
 * Defines how to respond to _ban.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Command | akairo.Command}
 */
class BanCommand extends bot_command_1.default {
    constructor() {
        super("ban", {
            aliases: ["ban", "b"],
            category: "mod",
            channel: "guild",
            clientPermissions: ["BAN_MEMBERS"],
            args: [
                {
                    id: "members",
                    type: (message) => message.mentions.members,
                },
            ],
        });
    }
    /**
     * Main execution procedure for _ban.
     *
     * @remarks
     * This is required by Akairo
     *
     * @param message - Will contain the {@link https://discord.js.org/#/docs/main/stable/class/Message | Message} object that hooked the command.
     * @param member - The {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} mentioned in the command call to be banned.
     * @returns - A message indicating that the {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} has been banned.
     */
    async exec(message, { members }) {
        return Promise.all(members.map((member) => member.ban())).then((handledMembers) => Promise.resolve(`${message.author.toString()} has banned ${handledMembers.toString()}.`));
    }
}
exports.default = BanCommand;
//# sourceMappingURL=ban.js.map