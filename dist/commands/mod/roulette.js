"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bot_command_1 = __importDefault(require("../../types/akairo-extensions/bot-command"));
/**
 * Defines how to respond to _roulette.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Command | akairo.Command}
 */
class RussianRouletteCommand extends bot_command_1.default {
    constructor() {
        super("roulette", {
            aliases: ["roulette", "r"],
            category: "mod",
            channel: "guild",
            clientPermissions: ["KICK_MEMBERS", "BAN_MEMBERS"],
            args: [{ id: "member", type: "member" }],
        });
    }
    /**
     * Main execution procedure for _roulette.
     *
     * @remarks
     * This is required by Akairo
     *
     * @param message - Will contain the {@link https://discord.js.org/#/docs/main/stable/class/Message | Message} object that hooked the command.
     * @param member - The {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} mentioned in the command call to be either kicked or banned.
     * @returns A message indicating that all handlers have been reloaded.
     */
    async exec(message, { member }) {
        const value = Math.random();
        const chance = value < 0.5;
        return (chance ? member.kick() : member.ban()).then((managedMember) => Promise.resolve(`<@${message.author.id}> has ${chance ? "kicked" : "banned"} **${managedMember.user.tag}** after rolling a ${value}`));
    }
}
exports.default = RussianRouletteCommand;
//# sourceMappingURL=roulette.js.map