"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bot_command_1 = __importDefault(require("../../types/akairo-extensions/bot-command"));
const verifiable_1 = __importDefault(require("../../types/verifiable"));
require("dotenv/config");
/**
 * Defines how to respond to _vegan and _nonvegan.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Command | akairo.Command}
 */
class VerifyCommand extends bot_command_1.default {
    constructor() {
        super("verify", {
            aliases: ["vegan", "nonvegan", "v", "nv"],
            category: "mod",
            channel: "guild",
            clientPermissions: ["MANAGE_ROLES", "MOVE_MEMBERS"],
            args: [{ id: "member", type: "member" }],
        });
    }
    /**
     * Main execution procedure for _vegan and _nonvegan.
     *
     * @remarks
     * This is required by Akairo
     *
     * @param message - Will contain the {@link https://discord.js.org/#/docs/main/stable/class/Message | Message} object that hooked the command.
     * @param member - The {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} mentioned in the command call to have their {@link https://discord.js.org/#/docs/main/stable/class/GuildMemberRoleManager | roles set}.
     * @returns A message indicating that all handlers have been reloaded.
     */
    async exec(message, { member }) {
        const isVegan = message.util?.parsed?.alias?.charAt(0).toLowerCase() === "v";
        return member.roles.cache.has(isVegan ? VerifyCommand.vegan : VerifyCommand.nonvegan)
            ? Promise.resolve("Already verified.")
            : member.roles.cache.has(VerifyCommand.verified)
                ? VerifyCommand.setStatus(member, isVegan).then((fullyVerifiedMember) => Promise.resolve(`<@${fullyVerifiedMember.id}> has had their role set to ${isVegan ? "vegan" : "nonvegan"} by <@${message.author.id}>`))
                : new verifiable_1.default(this.client, member)
                    .isVerifiable(message)
                    .then((unmutedMember) => VerifyCommand.verify(unmutedMember))
                    .then((verifiedMember) => VerifyCommand.setStatus(verifiedMember ?? member, isVegan))
                    .then((fullyVerifiedMember) => Promise.resolve(`<@${fullyVerifiedMember.id}> has been verified as a ${isVegan ? "vegan" : "nonvegan"} by <@${message.author.id}>`));
    }
}
exports.default = VerifyCommand;
VerifyCommand.vegan = process.env.vegan ?? ""; // TODO - read this stuff from Settings
VerifyCommand.nonvegan = process.env.nonvegan ?? ""; // TODO remove magic
VerifyCommand.verified = process.env.verified ?? ""; // TODO remove magic
VerifyCommand.generalVoice = process.env.generalVoice ?? ""; // TODO remove Magic
/**
 * Gives the user the verification {@link https://discord.js.org/#/docs/main/stable/class/Role | Role} and moves them to a new {@link https://discord.js.org/#/docs/main/stable/class/VoiceChannel | VoiceChannel}
 *
 * @param message - Will contain the {@link https://discord.js.org/#/docs/main/stable/class/Message | Message} object that hooked the command.
 * @param member - The {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} mentioned in the command call to be verified.
 * @returns A {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise | Promisified} {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} that is verified.
 */
VerifyCommand.verify = async function verifyGuildMember(member) {
    return member.roles.cache.has(VerifyCommand.verified)
        ? member
        : member.roles
            .add(VerifyCommand.verified)
            .then((verifiedMember) => verifiedMember.voice.channelID
            ? verifiedMember.voice.setChannel(VerifyCommand.generalVoice)
            : verifiedMember);
};
/**
 * Flushes relevant roles and replaces them with desired value.
 *
 * @param member - The {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} mentioned in the command call to have their {@link https://discord.js.org/#/docs/main/stable/class/GuildMemberRoleManager | roles set}.
 * @param isVegan - which {@link https://discord.js.org/#/docs/main/stable/class/Role | Role} the {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} should receive.
 * @returns A {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise | Promisified} {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} with the proper roles.
 */
VerifyCommand.setStatus = async function setGuildMemberRoles(member, isVegan) {
    return member.roles
        .remove([VerifyCommand.vegan, VerifyCommand.nonvegan])
        .then(async (flushedMember) => flushedMember.roles.add(isVegan ? VerifyCommand.vegan : VerifyCommand.nonvegan));
};
//# sourceMappingURL=verify.js.map