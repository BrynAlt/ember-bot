"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bot_command_1 = __importDefault(require("../../types/akairo-extensions/bot-command"));
/**
 * Defines how to respond to _sophist.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Command | akairo.Command}
 */
class SophistCommand extends bot_command_1.default {
    constructor() {
        super("sophist", {
            aliases: ["sophist", "soph", "cuck", "textcuck", "sophistvcbadger"],
            category: "mod",
            channel: "guild",
            clientPermissions: ["BAN_MEMBERS"],
            args: [{ id: "member", type: "member" }],
        });
    }
    /**
     * Main execution procedure for _sophist.
     *
     * @remarks
     * This is required by Akairo
     *
     * @param message - Will contain the {@link https://discord.js.org/#/docs/main/stable/class/Message | Message} object that hooked the command.
     * @param member - The {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} mentioned in the command call to be badgered.
     * @returns - A message indicating that the {@link https://discord.js.org/#/docs/main/stable/class/GuildMember | member} has been banned or saved.
     */
    async exec(message, { member }) {
        const sophist = message.util?.parsed?.alias?.charAt(0).toLowerCase() === "s";
        const end = new Date(Date.now() + (sophist ? 60000 : 180000)).getTime();
        const tick = sophist ? 10000 : 30000;
        return message.channel
            .send(`<@${member.id}>, you have ${sophist ? "one minute to return to" : "three minutes to join"} ${message.guild?.members.cache.get(message.author.id)?.voice.channel
            ?.name ?? `whichever channel <@${message.author.id}> is in`} or you will be permanently banned by <@${message.author.id}>. Try typing out a message and see what happens in the meantime.`)
            .then(() => setTimeout(function remindSophist() {
            // eslint-disable-next-line functional/no-conditional-statement
            if (!(message.member?.voice.channelID === member.voice.channelID)) {
                // eslint-disable-next-line functional/no-expression-statement
                void message.channel.send(`You have ${Math.round((end - Date.now()) / 1000)} seconds to come back to voice, <@${member.id}>`);
                // eslint-disable-next-line functional/no-conditional-statement
                if (end - Date.now() > tick)
                    // eslint-disable-next-line functional/no-expression-statement
                    setTimeout(remindSophist, tick);
                // eslint-disable-next-line functional/no-conditional-statement
                else if (end - Date.now() < 6000)
                    // eslint-disable-next-line functional/no-expression-statement
                    setTimeout(remindSophist, 1000);
            }
        }, tick))
            .then(() => this.client)
            .then((client) => client.on("message", function removeSpeakingPerms(filteredMessage) {
            // eslint-disable-next-line functional/no-conditional-statement
            if (end - Date.now() < 1000) {
                // eslint-disable-next-line functional/no-expression-statement
                client.off("message", removeSpeakingPerms);
                return;
            }
            // eslint-disable-next-line functional/no-conditional-statement
            if (filteredMessage.author.id === member.id)
                // eslint-disable-next-line functional/no-expression-statement
                void filteredMessage.delete();
        }))
            .then(() => new Promise((resolve) => setTimeout(resolve, end - Date.now())))
            .then(() => message.member?.voice.channelID === member.voice.channelID
            ? undefined
            : message.channel.send(`See ya, ${member.user.tag}!`))
            .then((inChannel) => (inChannel ? member.ban() : undefined))
            .then((handledMember) => Promise.resolve(`<@${message.author.id}> has ${handledMember === undefined
            ? `gotten ${member.user.tag} to ${sophist ? "return to" : "join"} the voice chat, so they've been spared for now.`
            : `banned **${member.user.tag}** for avoiding interaction.`}`));
    }
}
exports.default = SophistCommand;
//# sourceMappingURL=sophist.js.map