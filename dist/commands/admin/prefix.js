"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bot_command_1 = __importDefault(require("../../types/akairo-extensions/bot-command"));
class PrefixCommand extends bot_command_1.default {
    constructor() {
        super("prefix", {
            aliases: ["prefix"],
            category: "admin",
            args: [{ id: "prefix", type: "string", match: "rest", default: "?" }],
        });
    }
    /**
     * Main execution procedure for _prefix
     *
     * @remarks
     * This is required by Akairo
     *
     * @param message - Will contain the Message object that hooked the command
     * @param prefix - Will contain a string for the desire prefix for the guild
     * @returns An updated guild prefix
     */
    async exec(message, { prefix }) {
        return !message.guild
            ? Promise.reject(new Error("This command must be run in a guild."))
            : this.client.graphQLClient
                .mutate({
                mutation: this.client.graphql.constants.CREATE_OR_UPDATE_PREFIX,
                variables: {
                    guildID: message.guild.id,
                    prefix,
                },
                refetchQueries: [
                    {
                        query: this.client.graphql.constants.READ_PREFIX,
                        variables: { id: message.guild.id },
                    },
                ],
            })
                .then(() => Promise.resolve(`Updated the prefix for ${message.guild?.name ?? "the guild"} to ${prefix}`));
    }
}
exports.default = PrefixCommand;
//# sourceMappingURL=prefix.js.map