"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const logger_1 = require("../../logger");
class DiscordWarnListener extends discord_akairo_1.Listener {
    constructor() {
        super("discordWarn", {
            emitter: "client",
            event: "warn",
            category: "client",
        });
    }
    exec(info) {
        return this.client.logger.warn(info, {
            topic: logger_1.TOPICS.DISCORD,
            event: logger_1.EVENTS.DEBUG,
        });
    }
}
exports.default = DiscordWarnListener;
//# sourceMappingURL=warn-log.js.map