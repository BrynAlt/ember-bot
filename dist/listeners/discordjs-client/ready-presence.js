"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const logger_1 = require("../../logger");
const events_1 = require("../../logger/events");
/**
 * Defines how to set the presence of the bot as soon as we have a heartbeat from Discord
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Listener | akairo.Listener}
 */
class PresenceListener extends discord_akairo_1.Listener {
    constructor() {
        super("presence", {
            emitter: "client",
            category: "client",
            event: "ready",
        });
    }
    /**
     * Main execution procedure for setting the bot's {@link https://discord.js.org/#/docs/main/stable/class/Presence | presence}.
     *
     * @remarks
     * This is required by Akairo.
     *
     * @returns If client.config.commandCenter, a message sent to client.owner announcing the bot is active.
     */
    async exec() {
        return this.client.user
            ?.setPresence({
            activity: { name: "to the universe.", type: "LISTENING" },
        })
            .then((presence) => this.client.logger.verbose(`Presence set to ${presence.activities[0].type} ${presence.activities[0].name}`, {
            topic: logger_1.TOPICS.DISCORD,
            event: events_1.EVENTS.READY,
        }));
    }
}
exports.default = PresenceListener;
//# sourceMappingURL=ready-presence.js.map