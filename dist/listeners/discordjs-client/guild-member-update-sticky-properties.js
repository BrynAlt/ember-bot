"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bot_listener_1 = __importDefault(require("../../types/akairo-extensions/bot-listener"));
/**
 * Defines how to store properties for stickiness when a {@link https://discord.js.org/#/docs/main/stable/class/GuildMember| guildMember} is updated.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Listener | akairo.Listener}
 */
class StickyPropertyListener extends bot_listener_1.default {
    /**
     * Emitted on {@link https://discord.js.org/#/docs/main/stable/class/Client?scrollTo=e-guildMemberUpdate | client.guildMemberUpdate}
     */
    constructor() {
        super("stickyProperties", {
            emitter: "client",
            category: "client",
            event: "guildMemberUpdate",
        });
    }
    /**
     * Main execution procedure for handling the update by saving the {@link https://discord.js.org/#/docs/main/stable/class/GuildMember| guildMember} via upsert.
     *
     * @remarks
     * This is required by Akairo
     *
     * @param oldMember - Will contain the old {@link https://discord.js.org/#/docs/main/stable/class/GuildMember| guildMember}.
     * @param newMember - Will contain the new {@link https://discord.js.org/#/docs/main/stable/class/GuildMember| guildMember}, containing role and nickname changes.
     * @returns An upsert of the guildMember's object to the database.
     */
    async exec(oldMember, newMember) {
        return !(oldMember.roles === newMember.roles ||
            oldMember.nickname !== newMember.nickname)
            ? Promise.resolve()
            : Promise.resolve(); /* this.client.graphQLClient.mutate({
            mutation: this.client.graphql.constants.CREATE_OR_UPDATE_GUILDMEMBER,
            variables: {
              id: newMember.id,
              guildID: newMember.guild.id,
              member: newMember,
              user: newMember.user,
            },
            refetchQueries: [
              {
                query: this.client.graphql.constants.READ_GUILDMEMBER,
                variables: {
                  id: newMember.id,
                  guildID: newMember.guild.id,
                },
              },
            ],
          }); */
    }
}
exports.default = StickyPropertyListener;
//# sourceMappingURL=guild-member-update-sticky-properties.js.map