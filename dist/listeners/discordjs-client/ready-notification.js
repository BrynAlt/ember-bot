"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const logger_1 = require("../../logger");
/**
 * Defines how to announce the bot is active as soon as we have a heartbeat from Discord
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Listener | akairo.Listener}
 */
class ReadyNotificationListener extends discord_akairo_1.Listener {
    constructor() {
        super("readyNotification", {
            emitter: "client",
            category: "client",
            event: "ready",
        });
    }
    /**
     * Main execution procedure for the announcement.
     *
     * @remarks
     * This is required by Akairo.
     *
     *
     * @returns If client.config.commandCenter, a message sent to client.owner announcing the bot is active.
     */
    exec() {
        return this.client.logger.info(`${this.client.user?.tag ?? "Bot"} is now active as ${this.client.user?.id ?? "NaN"}.`, {
            topic: logger_1.TOPICS.DISCORD,
            event: logger_1.EVENTS.READY,
        });
    }
}
exports.default = ReadyNotificationListener;
//# sourceMappingURL=ready-notification.js.map