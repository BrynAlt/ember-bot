"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const logger_1 = require("../../logger");
class DiscrdDebugListener extends discord_akairo_1.Listener {
    constructor() {
        super("discordDebug", {
            emitter: "client",
            event: "debug",
            category: "client",
        });
    }
    exec(info) {
        return this.client.config.debugging
            ? this.client.logger.silly(info, {
                topic: logger_1.TOPICS.DISCORD,
                event: logger_1.EVENTS.DEBUG,
            })
            : undefined;
    }
}
exports.default = DiscrdDebugListener;
//# sourceMappingURL=debug-log.js.map