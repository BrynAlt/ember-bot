"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const logger_1 = require("../../logger");
class DiscordErrorListener extends discord_akairo_1.Listener {
    constructor() {
        super("unhandledRejection", {
            emitter: "process",
            event: "unhandledRejection",
            category: "process",
        });
    }
    exec(error) {
        return this.client.logger.warn(`${error.message}\n${error.stack ?? ""}`, {
            topic: logger_1.TOPICS.DISCORD,
            event: logger_1.EVENTS.DEBUG,
        });
    }
}
exports.default = DiscordErrorListener;
//# sourceMappingURL=unhandled-rejection.js.map