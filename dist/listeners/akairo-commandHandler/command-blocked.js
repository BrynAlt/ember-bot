"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const logger_1 = require("../../logger");
/**
 * Defines how to handle a command being blocked.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Listener | akairo.Listener}
 */
class CommandBlockedListener extends discord_akairo_1.Listener {
    constructor() {
        super("commandBlocked", {
            emitter: "commandHandler",
            category: "commandHandler",
            event: "commandBlocked",
        });
    }
    /**
     * Main execution procedure for handling {@link https://discord-akairo.github.io/#/docs/main/master/class/CommandHandler?scrollTo=e-commandBlocked | a command being blocked} by an {@link https://discord-akairo.github.io/#/docs/main/master/class/Inhibitor | inhibitor}.
     *
     * @remarks
     * This is required by Akairo.
     *
     * @param message - Will contain the {@link https://discord.js.org/#/docs/main/stable/class/Message | Message} object that hooked the command.
     * @param command - Will be the {@link https://discord-akairo.github.io/#/docs/main/master/class/Command | Command} called.
     * @param reason - The reason provided by the {@link https://discord-akairo.github.io/#/docs/main/master/class/Inhibitor | inhibitor}.
     * @returns If client.config.commandCenter, a message sent to client.owner announcing the bot is active.
     */
    exec(message, command, reason) {
        return this.client.logger.verbose(`Blocked ${command.id} in ${message.guild ? `${message.guild.name} (${message.guild.id})` : "DM"} with reason ${reason}`, { topic: logger_1.TOPICS.DISCORD_AKAIRO, event: logger_1.EVENTS.COMMAND_BLOCKED });
    }
}
exports.default = CommandBlockedListener;
//# sourceMappingURL=command-blocked.js.map