"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const logger_1 = require("../../logger");
/**
 * Defines how to set the presence of the bot as soon as we have a heartbeat from Discord
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Listener | akairo.Listener}
 */
class CommandErrorListener extends discord_akairo_1.Listener {
    constructor() {
        super("commandError", {
            emitter: "commandHandler",
            category: "commandHandler",
            event: "error",
        });
    }
    /**
     * Main execution procedure for handling {@link https://discord-akairo.github.io/#/docs/main/master/class/CommandHandler?scrollTo=e-error | a command finishing with an error}.
     *
     * @remarks
     * This is required by Akairo.
     *
     * @returns If client.config.commandCenter, a message sent to client.owner announcing the bot is active.
     */
    async exec(error, message, command) {
        // eslint-disable-next-line promise/no-promise-in-callback
        return message.channel
            .send(`Error occurred executing ${command.id}. ${error.message}`)
            .then(() => message.reactions.removeAll())
            .then((clearedMessage) => clearedMessage.react("⁉️"))
            .then(() => this.client.logger.prompt(error.message, {
            topic: logger_1.TOPICS.DISCORD_AKAIRO,
            event: logger_1.EVENTS.COMMAND_ERROR,
        }));
    }
}
exports.default = CommandErrorListener;
//# sourceMappingURL=command-error.js.map