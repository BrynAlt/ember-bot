"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
const discord_js_1 = require("discord.js");
const logger_1 = require("../../logger");
/**
 * Defines how to handle a command being finished.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Listener | akairo.Listener}
 */
class CommandFinishedListener extends discord_akairo_1.Listener {
    constructor() {
        super("commandFinished", {
            emitter: "commandHandler",
            category: "commandHandler",
            event: "commandFinished",
        });
    }
    /**
     * Main execution procedure for handling {@link https://discord-akairo.github.io/#/docs/main/master/class/CommandHandler?scrollTo=e-commandFinished | a command finishing}.
     *
     * @remarks
     * This is required by Akairo.
     *
     * @param message - Will contain the {@link https://discord.js.org/#/docs/main/stable/class/Message | Message} object that hooked the command.
     * @param command - Will be the {@link https://discord-akairo.github.io/#/docs/main/master/class/Command | Command} called.
     * @param commandArguments - The set of arguments discovered by akairo.
     * @returns If client.config.commandCenter, a message sent to client.owner announcing the bot is active.
     */
    async exec(message, command, commandArguments, returnValue) {
        return message.reactions
            .removeAll()
            .then((clearedMessage) => clearedMessage.react("✅"))
            .then(() => this.client.logger.verbose(`Finished ${command.id} on ${message.guild ? `${message.guild.name} (${message.guild.id})` : "DM"}${Object.keys(commandArguments).length > 0
            ? ` with arguments ${JSON.stringify(commandArguments)}`
            : ""}`, { topic: logger_1.TOPICS.DISCORD_AKAIRO, event: logger_1.EVENTS.COMMAND_FINISHED }))
            .then(() => returnValue)
            .then((content) => typeof content === "string"
            ? message.util?.send(content) ?? message.channel.send(content)
            : Promise.resolve(new discord_js_1.Message(this.client, command, message.channel)));
    }
}
exports.default = CommandFinishedListener;
//# sourceMappingURL=command-finished.js.map