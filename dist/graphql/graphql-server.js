"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = exports.graphqlServer = void 0;
/* eslint-disable functional/no-expression-statement */
const apollo_server_1 = require("apollo-server");
const logger_1 = require("../logger");
const context_1 = require("./context");
const schema_1 = require("./schema");
exports.graphqlServer = new apollo_server_1.ApolloServer({
    schema: schema_1.schema,
    context: context_1.createContext,
    tracing: process.env.NODE_ENV === "development",
});
exports.config = {
    api: {
        bodyParser: false,
    },
};
exports.graphqlServer
    .listen()
    .then((serverInfo) => logger_1.logger.prompt(`🚀 Server ready at  ${serverInfo.url}`, {
    topic: logger_1.TOPICS.APOLLO_SERVER,
    event: logger_1.EVENTS.READY,
}))
    .catch((error) => logger_1.logger.error(error.message, {
    topic: logger_1.TOPICS.APOLLO_SERVER,
    event: logger_1.EVENTS.ERROR,
}));
exports.default = exports.graphqlServer;
//# sourceMappingURL=graphql-server.js.map