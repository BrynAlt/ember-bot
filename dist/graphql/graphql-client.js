"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.graphql = exports.graphQLClient = void 0;
const client_1 = require("@apollo/client");
const error_1 = require("@apollo/client/link/error");
const cross_fetch_1 = __importDefault(require("cross-fetch"));
const graphql_tag_1 = __importDefault(require("graphql-tag"));
const logger_1 = require("../logger");
const errorLink = error_1.onError(({ graphQLErrors, networkError }) => {
    graphQLErrors?.forEach(({ message, locations, path }) => logger_1.logger.error(`Message: ${message}, Locations: ${locations?.toString() ?? "N/a"}, Path: ${path?.toString() ?? "N/a"}`, { topic: logger_1.TOPICS.APOLLO_CLIENT, event: logger_1.EVENTS.ERROR }));
    return !networkError
        ? undefined
        : logger_1.logger.error(networkError.message, {
            topic: logger_1.TOPICS.APOLLO_SERVER,
            event: logger_1.EVENTS.ERROR,
        });
});
const httpLink = new client_1.HttpLink({ uri: "http://localhost:4000/graphql", fetch: cross_fetch_1.default });
exports.graphQLClient = new client_1.ApolloClient({
    link: client_1.ApolloLink.from([errorLink, httpLink]),
    cache: new client_1.InMemoryCache(),
});
exports.default = exports.graphQLClient;
exports.graphql = {
    constants: {
        CREATE_OR_UPDATE_PREFIX: graphql_tag_1.default `
      mutation UpsertPrefix($guildID: String!, $prefix: String!) {
        upsertOneGuild(
          where: { id: $guildID }
          create: { id: $guildID, prefix: $prefix }
          update: { prefix: { set: $prefix } }
        ) {
          prefix
        }
      }
    `,
        READ_PREFIX: graphql_tag_1.default `
      query CachePrefix($id: String!) {
        guild(where: { id: $id }) {
          prefix
        }
      }
    `,
        CREATE_OR_UPDATE_GUILDMEMBER_ROLE: graphql_tag_1.default `
      mutation UpsertGuildMemberWithRole(
        $id: String!
        $guildID: String!
        $roleID: String!
        $roleName: String!
      ) {
        upsertOneGuildMember(
          create: {
            guild: {
              connectOrCreate: {
                create: {
                  id: $guildID
                },
                where: {
                  id: $guildID
                }
              }
            },
            user: {
              connectOrCreate: {
                create: {
                  id: $id
                },
                where: {
                  id: $id
                }
              }
            },
            roles: {
              connectOrCreate: {
                create: {
                  role: {
                    connectOrCreate: {
                      create: {
                        id: $roleID
                        name: $roleName
                      },
                      where: {
                        id: $roleID
                      }
                    }
                  }
                },
                where: {
                  guildMemberID: $id
                }
              }
            }
          },
          update: {
            roles: {
              upsert: {
                create: {
                  role: {
                    connectOrCreate: {
                      create: {
                        id: $roleID
                        name: $roleName
                      },
                      where :{
                        id: $roleID
                      }
                    }
                  }
                },
                update: {
                  role: {
                    upsert: {
                      create: {
                        id: $roleID
                        name: $roleName
                      },
                      update: {
                        name: {
                          set:  $roleName
                        }
                      }
                    }
                  }
                },
                where: {
                  guildMemberID: $id
                }
              }
            }
          },
          where: {
            id_guildID: {
              id: $id,
              guildID: $guildID
            }
          }
        ) {
          roles {
            role {
              id
            }
          }
        }
      }
    `,
        READ_GUILDMEMBER_ROLES: graphql_tag_1.default `
      query CacheMember($id: String!, $guildID: String!) {
        guildMember(where: { id_guildID: { id: $id, guildID: $guildID } }) {
          roles {
            role {
              id
            }
          }
        }
      }
    `,
    },
};
//# sourceMappingURL=graphql-client.js.map