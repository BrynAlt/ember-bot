"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable functional/no-expression-statement */
const nexus_1 = require("nexus");
const User = nexus_1.objectType({
    name: "User",
    definition(t) {
        t.model.id();
    },
});
exports.default = User;
//# sourceMappingURL=user.js.map