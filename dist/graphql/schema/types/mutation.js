"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable functional/no-expression-statement */
const nexus_1 = require("nexus");
const Mutation = nexus_1.mutationType({
    definition(t) {
        t.crud.createOneGuild();
        t.crud.deleteOneGuild();
        t.crud.updateOneGuild();
        t.crud.upsertOneGuild();
        t.crud.updateManyGuild();
        t.crud.deleteManyGuild();
        t.crud.createOneGuildMember();
        t.crud.deleteOneGuildMember();
        t.crud.updateOneGuildMember();
        t.crud.upsertOneGuildMember();
        t.crud.updateManyGuildMember();
        t.crud.deleteManyGuildMember();
        t.crud.createOneGuildMemberRoleIntermediate();
        t.crud.deleteOneGuildMemberRoleIntermediate();
        t.crud.updateOneGuildMemberRoleIntermediate();
        t.crud.upsertOneGuildMemberRoleIntermediate();
        t.crud.updateManyGuildMemberRoleIntermediate();
        t.crud.deleteManyGuildMemberRoleIntermediate();
        t.crud.createOneRole();
        t.crud.deleteOneRole();
        t.crud.updateOneRole();
        t.crud.upsertOneRole();
        t.crud.updateManyRole();
        t.crud.deleteManyRole();
        t.crud.updateManyGuildMemberRoleIntermediate();
    },
});
exports.default = Mutation;
//# sourceMappingURL=mutation.js.map