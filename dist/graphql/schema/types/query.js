"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable functional/no-expression-statement */
const nexus_1 = require("nexus");
const Query = nexus_1.queryType({
    definition(t) {
        t.crud.guild();
        t.crud.guilds();
        t.crud.guildMember();
        t.crud.guildMembers();
        t.crud.user();
        t.crud.users();
        t.crud.guildMemberRoleIntermediate();
        t.crud.guildMemberRoleIntermediates();
    },
});
exports.default = Query;
//# sourceMappingURL=query.js.map