"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable functional/no-expression-statement */
const nexus_1 = require("nexus");
const GuildMember = nexus_1.objectType({
    name: "GuildMember",
    definition(t) {
        t.model.id();
        t.model.guildID();
        t.model.member();
        t.model.roles();
    },
});
exports.default = GuildMember;
//# sourceMappingURL=guild-member.js.map