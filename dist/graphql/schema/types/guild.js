"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable functional/no-expression-statement */
const nexus_1 = require("nexus");
const Guild = nexus_1.objectType({
    name: "Guild",
    definition(t) {
        t.model.id();
        t.model.prefix();
        t.model.welcomeEnabled();
        t.model.welcomeMessage();
        t.model.members();
    },
});
exports.default = Guild;
//# sourceMappingURL=guild.js.map