"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.schema = void 0;
const nexus_1 = require("nexus");
const nexus_plugin_prisma_1 = require("nexus-plugin-prisma");
const path_1 = __importDefault(require("path"));
const logger_1 = require("../../logger");
const types = __importStar(require("./types"));
const generateSchema = Boolean(process.env.TS_NODE_DEV) || process.env.NODE_ENV === "development";
logger_1.logger.prompt(generateSchema
    ? "Detected we are in a TypeScript development environment.. Creating new TypeSafe Schema..."
    : "Creating Schema", {
    topic: logger_1.TOPICS.NEXUS,
    event: logger_1.EVENTS.INIT,
});
exports.schema = nexus_1.makeSchema({
    shouldExitAfterGenerateArtifacts: process.env.NEXUS_SHOULD_EXIT_AFTER_GENERATE_ARTIFACTS === "true",
    shouldGenerateArtifacts: generateSchema,
    types,
    contextType: {
        module: path_1.default.join(__dirname, "../context.ts"),
        export: "Context",
    },
    sourceTypes: {
        modules: [
            {
                module: require.resolve(".prisma/client/index.d.ts"),
                alias: "PrismaClient",
            },
        ],
        mapping: {
            Date: "Date",
            DateTime: "Date",
            Json: "JSON",
        },
    },
    plugins: [nexus_plugin_prisma_1.nexusPrisma({ experimentalCRUD: true })],
    outputs: {
        schema: path_1.default.join(__dirname, "../generated/schema.gen.graphql"),
        typegen: path_1.default.join(__dirname, "../generated/nexus-typegen.d.ts"),
    },
});
logger_1.logger.prompt("Schema Created...", {
    topic: logger_1.TOPICS.NEXUS,
    event: logger_1.EVENTS.READY,
});
exports.default = exports.schema;
//# sourceMappingURL=index.js.map