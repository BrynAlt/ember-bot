"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fuzzy_search_1 = __importDefault(require("fuzzy-search"));
class VoiceUtils {
}
exports.default = VoiceUtils;
/**
 *
 * @param guild - The {@link https://discord.js.org/#/docs/main/stable/class/Guild | Guild} to search in.
 * @param key - Either a {@link https://discord.js.org/#/docs/main/stable/typedef/Snowflake | Snowflake} or a string to search for.
 *
 * @returns A {@link https://discord.js.org/#/docs/main/stable/class/VoiceChannel | Voice Channel} if it exists in the {@link https://discord.js.org/#/docs/main/stable/class/Guild | Guild}, an {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error | Error} if the {@link https://discord.js.org/#/docs/main/stable/typedef/Snowflake | Snowflake} is not a {@link https://discord.js.org/#/docs/main/stable/class/VoiceChannel | Voice Channel}, otherwise {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/undefined | undefined}.
 */
VoiceUtils.resolveVC = async function resolveVoiceChannel(guild, key) {
    return Promise.resolve(!Number.isNaN(Number(key)) && guild.channels.cache.has(key)
        ? guild.channels.cache.get(key)?.type === "voice"
            ? guild.channels.cache.get(key)
            : Promise.reject(new Error("That channel is not a voice channel"))
        : new fuzzy_search_1.default(guild.channels.cache
            .filter((channel) => channel.type === "voice")
            .array(), ["name"]).search(key)[0]);
};
//# sourceMappingURL=voice.js.map