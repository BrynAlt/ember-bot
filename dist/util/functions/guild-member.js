"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class GuildMemberUtils {
    constructor() {
        /**
         * Check if `user` can manage the `target`.
         *
         * @param user - User who will be performing a moderation action
         * @param target - The member who is being checked as manageable
         * @see https://discord.js.org/#/docs/main/master/class/GuildMember?scrollTo=manageable Discord.js docs on `GuildMember.manageable`
         */
        this.manageable = function manageable(user, target) {
            return (target.user.id === user.guild.ownerID ||
                target.user.id === user.user.id ||
                user.user.id !== user.guild.ownerID ||
                user.roles.highest.comparePositionTo(target.roles.highest) > 0);
        };
    }
}
exports.default = GuildMemberUtils;
//# sourceMappingURL=guild-member.js.map