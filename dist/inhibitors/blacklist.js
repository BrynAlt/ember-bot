"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
/**
 * Defines how to block commands from users on the global or server blacklists.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Inhibitor | akairo.Inhibitor}
 */
class BlacklistInhibitor extends discord_akairo_1.Inhibitor {
    constructor() {
        super("blacklist", {
            reason: "blacklist",
        });
        this.blacklist = [];
    }
    /**
     * Main execution procedure for evaluating if a given {@link https://discord.js.org/#/docs/main/stable/class/Message | message} is from a blacklisted user.
     *
     * @remarks
     * This is required by Akairo.
     *
     * @param message - the message under evaluation
     * @param command - the {@link https://discord-akairo.github.io/#/docs/main/master/class/Command | command} that the {@link https://discord-akairo.github.io/#/docs/main/master/class/CommandHandler | command handler} has parsed from the message.
     * @returns Whether the author of the message is on the global blacklist or is on the server blacklist.
     */
    exec(message) {
        return this.blacklist.includes(message.author.id);
    }
}
exports.default = BlacklistInhibitor;
//# sourceMappingURL=blacklist.js.map