"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
require("dotenv/config");
/**
 * Defines how to block commands in the mod category from being run by users without the server's set moderator roles.
 *
 * @remarks
 *
 * @see {@link https://discord-akairo.github.io/#/docs/main/master/class/Inhibitor | akairo.Inhibitor}
 */
class ModeratorOnlyInhibitor extends discord_akairo_1.Inhibitor {
    constructor() {
        super("modOnly", {
            reason: "Command is in the 'mod' category, but was run by a non-moderator. Please expand moderator roles or give the user a moderator role.",
        });
        this.modRoles = JSON.parse(process.env.modRoles ?? ""); // TODO read from server settings
    }
    /**
     * Main execution procedure for evaluating if a given {@link https://discord.js.org/#/docs/main/stable/class/Message | message} is qualified for commands in the mod category.
     *
     * @remarks
     * This is required by Akairo.
     *
     * @param message - the message under evaluation
     * @param command - the {@link https://discord-akairo.github.io/#/docs/main/master/class/Command | command} that the {@link https://discord-akairo.github.io/#/docs/main/master/class/CommandHandler | command handler} has parsed from the message.
     * @returns Whether the author of the message has a role specified in Settings.modRoles
     */
    exec(message, command) {
        return (command.categoryID === "mod" &&
            !message.member?.roles.cache.some((role) => this.modRoles.includes(role.id)) &&
            !message.member?.permissions.has("ADMINISTRATOR"));
    }
}
exports.default = ModeratorOnlyInhibitor;
//# sourceMappingURL=moderator-only.js.map