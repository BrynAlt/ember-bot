"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable functional/no-expression-statement */
const winston_transport_1 = __importDefault(require("winston-transport"));
class NullTransport extends winston_transport_1.default {
    constructor(options) {
        super(options);
    }
    log(info, callback) {
        callback(this);
        return info;
    }
}
exports.default = NullTransport;
//# sourceMappingURL=null-transport.js.map