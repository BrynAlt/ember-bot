"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TOPICS = void 0;
var TOPICS;
(function (TOPICS) {
    TOPICS["APOLLO_CLIENT"] = "APOLLO_CLIENT";
    TOPICS["APOLLO_SERVER"] = "APOLLO_SERVER";
    TOPICS["DISCORD"] = "DISCORD";
    TOPICS["DISCORD_AKAIRO"] = "DISCORD_AKAIRO";
    TOPICS["METRICS"] = "METRICS";
    TOPICS["NEXUS"] = "NEXUS";
    TOPICS["RPC"] = "RPC";
    TOPICS["UNHANDLED_REJECTION"] = "UNHANDLED_REJECTION";
})(TOPICS = exports.TOPICS || (exports.TOPICS = {}));
exports.default = TOPICS;
//# sourceMappingURL=topics.js.map