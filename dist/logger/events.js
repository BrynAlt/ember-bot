"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EVENTS = void 0;
var EVENTS;
(function (EVENTS) {
    EVENTS["COMMAND_BLOCKED"] = "COMMAND_BLOCKED";
    EVENTS["COMMAND_CANCELLED"] = "COMMAND_CANCELLED";
    EVENTS["COMMAND_ERROR"] = "COMMAND_ERROR";
    EVENTS["COMMAND_FINISHED"] = "COMMAND_FINISHED";
    EVENTS["COMMAND_STARTED"] = "COMMAND_STARTED";
    EVENTS["CONNECT"] = "CONNECT";
    EVENTS["DEBUG"] = "DEBUG";
    EVENTS["DESTROY"] = "DESTROY";
    EVENTS["DISCONNECT"] = "DISCONNECT";
    EVENTS["ERROR"] = "ERROR";
    EVENTS["IDENTIFY"] = "IDENTIFY";
    EVENTS["INIT"] = "INIT";
    EVENTS["LOCKDOWN"] = "LOCKDOWN";
    EVENTS["MESSAGE_BLOCKED"] = "MESSAGE_BLOCKED";
    EVENTS["MUTE"] = "MUTE";
    EVENTS["READY"] = "READY";
    EVENTS["WARN"] = "WARN";
})(EVENTS = exports.EVENTS || (exports.EVENTS = {}));
exports.default = EVENTS;
//# sourceMappingURL=events.js.map