"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable functional/no-expression-statement */
/* eslint-disable functional/immutable-data */
/* eslint-disable functional/prefer-readonly-type */
const discord_js_1 = require("discord.js");
const os_1 = __importDefault(require("os"));
const winston_transport_1 = __importDefault(require("winston-transport"));
const colors_1 = require("../types/colors");
class DiscordTransport extends winston_transport_1.default {
    constructor(options) {
        super(options);
        this.webhook =
            options.discord === false
                ? undefined
                : new discord_js_1.WebhookClient(options.webhookID, options.webhookToken);
    }
    /**
     * Function exposed to winston to be called when logging messages
     * @param info - Log message from winston
     * @param callback - Callback to winston to complete the log
     */
    async log(info, callback) {
        return !this.webhook
            ? Promise.resolve().finally(() => callback())
            : this.webhook
                .send(new discord_js_1.MessageEmbed()
                .setTitle(info.level.toUpperCase())
                .setAuthor(info.label)
                .setDescription(info.message)
                .setColor(DiscordTransport.colorCodes[info.level])
                .addField("Host", os_1.default.hostname()))
                .finally(() => callback());
    }
}
exports.default = DiscordTransport;
/** Available colors for discord messages */
DiscordTransport.colorCodes = {
    error: colors_1.COLORS.DARK_RED,
    warn: colors_1.COLORS.RED,
    help: colors_1.COLORS.DARK_ORANGE,
    data: colors_1.COLORS.YELLOW,
    info: colors_1.COLORS.DARK_GREEN,
    debug: colors_1.COLORS.GREEN,
    prompt: colors_1.COLORS.DARK_BLUE,
    verbose: colors_1.COLORS.DARK_BLUE,
    input: colors_1.COLORS.DARK_PURPLE,
    silly: colors_1.COLORS.PURPLE,
};
//# sourceMappingURL=discord-transport.js.map