"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_akairo_1 = require("discord-akairo");
class BotCommand extends discord_akairo_1.Command {
    constructor() {
        super(...arguments);
        /**
         * Although the message utility is turned on in Ember's {@link BotClient}, TypeScript has no way of knowing so. This function avoids the compiler complaining, as well as provides for the edge case where the message utility is turned off. It will first attempt to use the utility send function, then fallback ont he default DiscordJS method.
         * @param message - The message that hooked the command.
         * @param content - The content of the message to be sent.
         * @returns - A Promisified message with the content desired.
         */
        this.send = async function sendMessageInCommandChannel(message, content) {
            return message.util?.send(content) ?? message.channel.send(content);
        };
    }
}
exports.default = BotCommand;
//# sourceMappingURL=bot-command.js.map