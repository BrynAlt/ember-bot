/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpsertGuildMemberWithRole
// ====================================================

export interface UpsertGuildMemberWithRole_upsertOneGuildMember_roles_role {
  __typename: "Role";
  id: string;
}

export interface UpsertGuildMemberWithRole_upsertOneGuildMember_roles {
  __typename: "GuildMemberRoleIntermediate";
  role: UpsertGuildMemberWithRole_upsertOneGuildMember_roles_role | null;
}

export interface UpsertGuildMemberWithRole_upsertOneGuildMember {
  __typename: "GuildMember";
  roles: UpsertGuildMemberWithRole_upsertOneGuildMember_roles[];
}

export interface UpsertGuildMemberWithRole {
  upsertOneGuildMember: UpsertGuildMemberWithRole_upsertOneGuildMember;
}

export interface UpsertGuildMemberWithRoleVariables {
  id: string;
  guildID: string;
  roleID: string;
  roleName: string;
}
